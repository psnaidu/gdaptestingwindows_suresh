Code Description:
This code uses a logistic regression model, and historical hail stats from Australia to predict the chance of hail on any given day & month.

The model uses variables: Day, Month 

Code Snip: 
#Logistic Regression
fit <- glm(Hail~Month+Day,
           data=train,family = binomial(link='logit'))

#Generating predictions
probtable$prob <- predict(fit, type = "response", probtable )
probtable$outcome <- ifelse(probtable$prob > 0.75,"High chance of hail","Low chance of hail")

Instructions:
Open QBE.Rproj, select "server" tab, select "Run Application"

Demo notes:
High chance of hail dates:
     Month Day State      prob             outcome
281      2  10   NSW 0.7910521 High chance of hail
282      2  10   VIC 0.7910521 High chance of hail
283      2  10    SA 0.7910521 High chance of hail
284      2  10   QLD 0.7910521 High chance of hail
285      2  10    NT 0.7910521 High chance of hail
286      2  10   TAS 0.7910521 High chance of hail
287      2  10    WA 0.7910521 High chance of hail
645      4   3   NSW 0.8260311 High chance of hail
646      4   3   VIC 0.8260311 High chance of hail
647      4   3    SA 0.8260311 High chance of hail
648      4   3   QLD 0.8260311 High chance of hail
649      4   3    NT 0.8260311 High chance of hail
650      4   3   TAS 0.8260311 High chance of hail
651      4   3    WA 0.8260311 High chance of hail
666      4   6   NSW 0.8673640 High chance of hail
667      4   6   VIC 0.8673640 High chance of hail
668      4   6    SA 0.8673640 High chance of hail
669      4   6   QLD 0.8673640 High chance of hail
670      4   6    NT 0.8673640 High chance of hail
671      4   6   TAS 0.8673640 High chance of hail
672      4   6    WA 0.8673640 High chance of hail
680      4   8   NSW 0.8984529 High chance of hail
681      4   8   VIC 0.8984529 High chance of hail
682      4   8    SA 0.8984529 High chance of hail
683      4   8   QLD 0.8984529 High chance of hail
684      4   8    NT 0.8984529 High chance of hail
685      4   8   TAS 0.8984529 High chance of hail
686      4   8    WA 0.8984529 High chance of hail
687      4   9   NSW 0.8124826 High chance of hail
688      4   9   VIC 0.8124826 High chance of hail
689      4   9    SA 0.8124826 High chance of hail
690      4   9   QLD 0.8124826 High chance of hail
691      4   9    NT 0.8124826 High chance of hail
692      4   9   TAS 0.8124826 High chance of hail
693      4   9    WA 0.8124826 High chance of hail
694      4  10   NSW 0.9802965 High chance of hail
695      4  10   VIC 0.9802965 High chance of hail
696      4  10    SA 0.9802965 High chance of hail
697      4  10   QLD 0.9802965 High chance of hail
698      4  10    NT 0.9802965 High chance of hail
699      4  10   TAS 0.9802965 High chance of hail
700      4  10    WA 0.9802965 High chance of hail
708      4  12   NSW 0.9653978 High chance of hail
709      4  12   VIC 0.9653978 High chance of hail
710      4  12    SA 0.9653978 High chance of hail
711      4  12   QLD 0.9653978 High chance of hail
712      4  12    NT 0.9653978 High chance of hail
713      4  12   TAS 0.9653978 High chance of hail
714      4  12    WA 0.9653978 High chance of hail
715      4  13   NSW 0.9391344 High chance of hail
716      4  13   VIC 0.9391344 High chance of hail
717      4  13    SA 0.9391344 High chance of hail
718      4  13   QLD 0.9391344 High chance of hail
719      4  13    NT 0.9391344 High chance of hail
720      4  13   TAS 0.9391344 High chance of hail
721      4  13    WA 0.9391344 High chance of hail
729      4  15   NSW 0.7812927 High chance of hail
730      4  15   VIC 0.7812927 High chance of hail
731      4  15    SA 0.7812927 High chance of hail
732      4  15   QLD 0.7812927 High chance of hail
733      4  15    NT 0.7812927 High chance of hail
734      4  15   TAS 0.7812927 High chance of hail
735      4  15    WA 0.7812927 High chance of hail
778      4  22   NSW 0.9261564 High chance of hail
779      4  22   VIC 0.9261564 High chance of hail
780      4  22    SA 0.9261564 High chance of hail
781      4  22   QLD 0.9261564 High chance of hail
782      4  22    NT 0.9261564 High chance of hail
783      4  22   TAS 0.9261564 High chance of hail
784      4  22    WA 0.9261564 High chance of hail
785      4  23   NSW 0.8828254 High chance of hail
786      4  23   VIC 0.8828254 High chance of hail
787      4  23    SA 0.8828254 High chance of hail
788      4  23   QLD 0.8828254 High chance of hail
789      4  23    NT 0.8828254 High chance of hail
790      4  23   TAS 0.8828254 High chance of hail
791      4  23    WA 0.8828254 High chance of hail
806      4  26   NSW 0.8180729 High chance of hail
807      4  26   VIC 0.8180729 High chance of hail
808      4  26    SA 0.8180729 High chance of hail
809      4  26   QLD 0.8180729 High chance of hail
810      4  26    NT 0.8180729 High chance of hail
811      4  26   TAS 0.8180729 High chance of hail
812      4  26    WA 0.8180729 High chance of hail
820      4  28   NSW 0.8740920 High chance of hail
821      4  28   VIC 0.8740920 High chance of hail
822      4  28    SA 0.8740920 High chance of hail
823      4  28   QLD 0.8740920 High chance of hail
824      4  28    NT 0.8740920 High chance of hail
825      4  28   TAS 0.8740920 High chance of hail
826      4  28    WA 0.8740920 High chance of hail
827      4  29   NSW 0.9382724 High chance of hail
828      4  29   VIC 0.9382724 High chance of hail
829      4  29    SA 0.9382724 High chance of hail
830      4  29   QLD 0.9382724 High chance of hail
831      4  29    NT 0.9382724 High chance of hail
832      4  29   TAS 0.9382724 High chance of hail
833      4  29    WA 0.9382724 High chance of hail
1121     6  10   NSW 0.8863753 High chance of hail
1122     6  10   VIC 0.8863753 High chance of hail
1123     6  10    SA 0.8863753 High chance of hail
1124     6  10   QLD 0.8863753 High chance of hail
1125     6  10    NT 0.8863753 High chance of hail
1126     6  10   TAS 0.8863753 High chance of hail
1127     6  10    WA 0.8863753 High chance of hail
1135     6  12   NSW 0.8139378 High chance of hail
1136     6  12   VIC 0.8139378 High chance of hail
1137     6  12    SA 0.8139378 High chance of hail
1138     6  12   QLD 0.8139378 High chance of hail
1139     6  12    NT 0.8139378 High chance of hail
1140     6  12   TAS 0.8139378 High chance of hail
1141     6  12    WA 0.8139378 High chance of hail
All else are low

